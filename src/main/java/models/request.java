package models;

import java.util.List;

@lombok.Data
public class request {
    private String name;
    private String url;
    private String method;
    private List<keyvalue> headers;
    private List<keyvalue> cookies;
    private String postdata;
    private String type;
}

