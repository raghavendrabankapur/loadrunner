package reports;

public class Reporters {
    public static void CsvWriter(String path, String data, boolean needNewLine) throws java.io.IOException {
        java.io.File file = new java.io.File(path);
        if (!file.exists()) file.createNewFile();

        java.io.FileWriter csvWriter = new java.io.FileWriter(path,true);
        csvWriter.append(data);
        if (needNewLine)
            csvWriter.append("\n");
        else
            csvWriter.append(",");
        csvWriter.close();
    }

    private static void CreateHeader(String path) throws java.io.IOException {
        java.io.File file = new java.io.File(path);
        if (!file.exists()) file.createNewFile();
        java.io.FileWriter csvWriter = new java.io.FileWriter(path,true);
        csvWriter.append("timestamp,");
        csvWriter.append("url,");
        csvWriter.append("status,");
        csvWriter.append("duration");
        csvWriter.append("\n");
        csvWriter.close();
    }

    public static void Report(String path) throws java.io.IOException {
        Reporters.CreateHeader(path);
    }
}
