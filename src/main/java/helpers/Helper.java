package helpers;

public class Helper {
    public  static  boolean isNullOrEmpty(String str){
        return str == null || str.trim().isEmpty();
    }
}
